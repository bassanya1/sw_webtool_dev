
class LocatorsLoginPage():
    #XPATH Selectors for all elements
    LOGIN_FIELD = "/html/body/div[1]/form/div[4]/table[1]/tbody/tr/td/div/div/div[2]/table/tbody/tr[1]/td/div/div/div/div[2]/table/tbody/tr/td/table/tbody/tr/td[2]/input"
    PASS_FIELD = "/html/body/div[1]/form/div[4]/table[1]/tbody/tr/td/div/div/div[2]/table/tbody/tr[1]/td/div/div/div/div[3]/table/tbody/tr/td/table/tbody/tr/td[2]/input"
    LOGIN_BUTTON = "/html/body/div[1]/form/div[4]/table[1]/tbody/tr/td/div/div/div[2]/table/tbody/tr[3]/td/div/div/div/ul/li/a"
    LOGIN_ON_LOGED_PAGE = "/html/body/form/div[6]/div/table/tbody/tr[1]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/table/tbody/tr/td/li[3]/div/div/div/div/ul/li[5]/a/span"


class LocatorsMainPage():
    # XPATH Selectors for all elements
    MP_USERS = "/html/body/form/div[6]/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div[3]/div[1]/div/ul/li[1]/div[2]/span"
    MP_OTHER_CATALOGS = "/html/body/form/div[6]/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div[3]/div[1]/div/ul/li[2]/div[2]/span"
    MP_SALES_STUCTURE = "/html/body/form/div[6]/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div[3]/div[1]/div/ul/li[3]/div[2]/span"
    MP_OUTLETS = "/html/body/form/div[6]/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div[3]/div[1]/div/ul/li[4]/div[2]/span"
    MP_PRODUCTS = "/html/body/form/div[6]/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div[3]/div[1]/div/ul/li[5]/div[2]/span"
    MP_DOCUMENTS = "/html/body/form/div[6]/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div[3]/div[1]/div/ul/li[6]/div[2]/span"
    MP_PLAN_AND_ANALYSE = "/html/body/form/div[6]/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div[3]/div[1]/div/ul/li[7]/div[2]/span"
    MP_REPORTS = "/html/body/form/div[6]/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div[3]/div[1]/div/ul/li[8]/div[2]/span"
    MP_DATA_EXCHANGE = "/html/body/form/div[6]/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div[3]/div[1]/div/ul/li[9]/div[2]/span"