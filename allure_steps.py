class LoginPageAllure():
    OPEN_URL = "Open Login page"
    SEARCH_LOGIN_FIELD = "Search login field"
    ENTER_LOGIN = "Enter login"
    SEARCH_PASS_FIELD = "Search password field"
    ENTER_PASS = "Enter password"
    SEARCH_BUTTON = "Search Login button for submit"
    CLICK_BUTTON = "Click on Login button"

class MainPageAllure():
    USERS_DIR = "Directory 'Users' is on page and named 'Пользователи'"
    OTHER_CATALOGS_DIR = "Directory 'Other catalogs' is on page and named 'Другие справочники'"
    SALES_STUCTURE_DIR = "Directory 'Sales structure' is on page and named 'Структура продаж'"
    OUTLETS_DIR = "Directory 'Outlets' is on page and named 'Торговые точки'"
    PRODUCTS_DIR = "Directory 'Products' is on page and named 'Продукция'"
    DOCUMENTS_DIR = "Directory 'Documents' is on page and named 'Документы'"
    PLAN_AND_ANALYSE_DIR = "Directory 'Plan and analyse' is on page and named 'План/Анализ'"
    REPORTS_DIR = "Directory 'Report' is on page and named 'Отчеты'"
    DATA_EXCHANGE_DIR = "Directory 'Data exchange' is on page and named 'Обмен данными'"

